#!/bin/bash

set -e

chmod -R 777 ${DUMP_DIRECTORY}
rm -rf ${DUMP_DIRECTORY}/*/

echo 'Extracting archives....'
cd ${DUMP_DIRECTORY}
tar -zxf ststransutf8_light_*.tar.gz
tar -zxf portailsts_light_*.tar.gz

echo 'Create databases....'

mysqloptions="--defaults-extra-file=/etc/mysql/defaults.cnf"

mysql ${mysqloptions} -e "
DROP DATABASE IF EXISTS portailsts;
DROP DATABASE IF EXISTS ststransutf8;

CREATE DATABASE portailsts CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE DATABASE ststransutf8 CHARACTER SET utf8 COLLATE utf8_general_ci;

GRANT ALL PRIVILEGES ON *.* TO 'ststrans'@'%' IDENTIFIED BY '${MYSQL_PASSWORD}';
GRANT ALL PRIVILEGES ON *.* TO 'ststransutf8'@'%' IDENTIFIED BY '${MYSQL_PASSWORD}';
GRANT ALL PRIVILEGES ON *.* TO 'portailsts'@'%' IDENTIFIED BY '${MYSQL_PASSWORD}';

CREATE USER IF NOT EXISTS 'multi_admin'@'localhost' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}';
SET PASSWORD FOR 'multi_admin'@'localhost' = '${MYSQL_ROOT_PASSWORD}';
FLUSH PRIVILEGES;

GRANT SHUTDOWN ON *.* TO 'multi_admin'@'localhost' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}';
"


echo 'Import portailsts data....'

cd ${DUMP_DIRECTORY}/portailsts

mysql ${mysqloptions} -e "
SET GLOBAL FOREIGN_KEY_CHECKS=0;
SET GLOBAL UNIQUE_CHECKS=0;
"

for filename in *.sql
do
    echo "Import ${filename}...."
    mysql ${mysqloptions} portailsts < $filename
done

echo 'Import ststransutf8 data....'

cd ${DUMP_DIRECTORY}/ststransutf8

echo "Import ststransutf8_light.sql...."
mysql ${mysqloptions} ststransutf8 < ststransutf8_light.sql

echo "Dropping indexes..."
mysql ${mysqloptions} ststransutf8 < drop_indexes.sql

for filename in *.txt
do
    echo "Import ${filename}...."
    mysqlimport ${mysqloptions} --local ststransutf8 ${DUMP_DIRECTORY}/ststransutf8/$filename
done

echo "Adding indexes..."
mysql ${mysqloptions} ststransutf8 < add_indexes.sql

mysql -uroot -p${MYSQL_ROOT_PASSWORD} -h 127.0.0.1 -e "
SET GLOBAL UNIQUE_CHECKS=1;
SET GLOBAL FOREIGN_KEY_CHECKS=1;
"

echo "Import triggers...."
mysql ${mysqloptions} ststransutf8 < triggers.sql
