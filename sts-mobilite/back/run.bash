#!/bin/bash

python3 -m venv py-env
source py-env/bin/activate

pip install -r requirements.txt

flask db upgrade

gunicorn app:app --preload --bind=0.0.0.0
