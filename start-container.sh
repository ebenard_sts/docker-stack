#!/bin/bash

compose_files=(
  docker-compose.yml
  docker-compose.elk.yml
  docker-compose.elic.yml
  docker-compose.local-database.yml
  docker-compose.login.yml
)
DIR="$( cd "$( dirname "$0" )" && pwd )/"
docker compose ${compose_files[@]/#/-f ${DIR}} up -d --force-recreate $@
