version: '3'
services:
    traefik:
        image: traefik:latest
        container_name: traefik
        networks:
            - traefik
        ports:
            - "80:80"
            - "443:443/tcp"
            - "443:443/udp"
        volumes:
            - /var/run/docker.sock:/var/run/docker.sock:ro
            - ./traefik/traefik.yml:/etc/traefik/traefik.yml:ro
            - ./traefik/config.yml:/etc/traefik/config.yml:ro
            - ./certs:/etc/certs:ro
        labels:
            - traefik.enable=true
            - traefik.http.routers.traefik=true

    legacy.docker.localhost:
        image: 412095673898.dkr.ecr.eu-west-1.amazonaws.com/base-php72-dev:latest
        container_name: legacy
        networks:
            - traefik
        environment:
            PHP_IDE_CONFIG: serverName=legacy.docker.localhost
            PHP_SAVE_PATH_CONFIG: tcp://memcache:11211?persistent=1&amp;weight=1&amp;timeout=1&amp;retry_interval=15
            XDEBUG_REMOTE_HOST: ${XDEBUG_REMOTE_HOST}
            APACHE_DOCUMENT_ROOT: /var/www/html/V3_www_prod
            SYMFONY_ENV: dev
            ALLOW_OVERRIDE_APACHE: All
            SYMFONY_INDEX_FILE: index.php
            PHP_SESSION_AUTOSTART: 1
        volumes:
            - ${SOURCES_PATH}/monolith/stsweb:/var/www/html
            - ${SSH_KEY_PATH}:/var/www/.ssh
            - ${SOURCES_PATH}/keys:/var/www/keys
            - ${SOURCES_PATH}/auth.json:/root/.composer/auth.json
            - ${SOURCES_PATH}/auth.json:/var/www/.config/composer/auth.json
        labels:
            - traefik.enable=true
            - traefik.http.routers.legacy.rule=Host(`legacy.docker.localhost`)
            - traefik.http.routers.legacy.tls=true
            - traefik.http.services.legacy.loadbalancer.server.port=443
        working_dir: /var/www/html
        extra_hosts:
            - host.docker.internal:host-gateway

    api.docker.localhost:
        image: 412095673898.dkr.ecr.eu-west-1.amazonaws.com/base-php72-dev:latest
        container_name: api
        networks:
            - traefik
        environment:
            PHP_IDE_CONFIG: serverName=api.docker.localhost
            PHP_SAVE_PATH_CONFIG: tcp://memcache:11211?persistent=1&amp;weight=1&amp;timeout=1&amp;retry_interval=15
            XDEBUG_REMOTE_HOST: ${XDEBUG_REMOTE_HOST}
            SYMFONY_ENV: dev
            APACHE_DOCUMENT_ROOT: /var/www/html/web
            ALLOW_OVERRIDE_APACHE: All
            SYMFONY_INDEX_FILE: index.php
            PHP_SESSION_AUTOSTART: 1
        volumes:
            - ${SOURCES_PATH}/monolith/api-common:/var/www/html
            - ${SOURCES_PATH}/monolith/stsweb/stsdump:/var/www/stsweb/stsdump
            - ${SSH_KEY_PATH}:/var/www/.ssh
            - ${SOURCES_PATH}/keys:/var/www/keys
            - ${SOURCES_PATH}/auth.json:/root/.composer/auth.json
            - ${SOURCES_PATH}/auth.json:/var/www/.config/composer/auth.json
        labels:
            - traefik.enable=true
            - traefik.http.routers.api.rule=Host(`api.docker.localhost`)
            - traefik.http.routers.api.tls=true
            - traefik.http.services.api.loadbalancer.server.port=443
        working_dir: /var/www/html
        extra_hosts:
            - host.docker.internal:host-gateway

    portail.docker.localhost:
        image: 412095673898.dkr.ecr.eu-west-1.amazonaws.com/base-php72-dev:latest
        container_name: portail
        networks:
            - traefik
        environment:
            PHP_IDE_CONFIG: serverName=portail.docker.localhost
            PHP_SAVE_PATH_CONFIG: tcp://memcache:11211?persistent=1&amp;weight=1&amp;timeout=1&amp;retry_interval=15
            XDEBUG_REMOTE_HOST: ${XDEBUG_REMOTE_HOST}
            SYMFONY_ENV: dev
            APACHE_DOCUMENT_ROOT: /var/www/html/web
            ALLOW_OVERRIDE_APACHE: All
            SYMFONY_INDEX_FILE: index.php
            PHP_SESSION_AUTOSTART: 1
        volumes:
            - ${SOURCES_PATH}/monolith/portailsts:/var/www/html
            - ${SOURCES_PATH}/monolith/stsweb/stsdump:/var/www/stsweb/stsdump
            - ${SSH_KEY_PATH}:/var/www/.ssh
            - ${SOURCES_PATH}/keys:/var/www/keys
            - ${SOURCES_PATH}/auth.json:/root/.composer/auth.json
            - ${SOURCES_PATH}/auth.json:/var/www/.config/composer/auth.json
        labels:
            - traefik.enable=true
            - traefik.http.routers.portail.rule=Host(`portail.docker.localhost`)
            - traefik.http.routers.portail.tls=true
            - traefik.http.services.portail.loadbalancer.server.port=443
        working_dir: /var/www/html
        extra_hosts:
            - host.docker.internal:host-gateway

    database:
        build:
            context: ./database
            args:
                MYSQL_ROOT_PASSWORD: root
        networks:
            - traefik
        container_name: database
        ports:
            - "3306:3306"
        environment:
            MYSQL_ROOT_PASSWORD: root
            MYSQL_PASSWORD:
            DUMP_DIRECTORY: /tmp/mysql/backup
        volumes:
            - /var/www/mysql/data:/var/lib/mysql
            - /var/www/mysql/backup:/tmp/mysql/backup

    memcache:
        image: memcached:latest
        networks:
            - traefik
        container_name: memcache

    mailer:
        image: mailhog/mailhog:latest
        container_name: mailer
        networks:
            - traefik
        labels:
            - traefik.enable=true
            - traefik.http.routers.mailer.rule=Host(`mailer.docker.localhost`)
            - traefik.http.routers.mailer.tls=true
            - traefik.http.services.mailer.loadbalancer.server.port=8025

    node:
        image: node:12.22.12
        container_name: node
        networks:
            - traefik
        volumes:
            - ${SOURCES_PATH}/monolith/portailsts:/var/www/html
        working_dir: /var/www/html
        command: bash -c "npm ci && npm run start"
        labels:
            - traefik.enable=true
            - traefik.http.routers.node.rule=Host(`node.docker.localhost`)
            - traefik.http.routers.node.tls=true
            - traefik.http.services.node.loadbalancer.server.port=8080

    rabbitmq:
        image: rabbitmq:3.11.1-management
        container_name: rabbitmq
        networks:
            - traefik
        environment:
            RABBITMQ_DEFAULT_USER: sts_rmq_test
            RABBITMQ_DEFAULT_PASS: sts_rmq_test
            RABBITMQ_DEFAULT_VHOST: /
        volumes:
            - ./rabbitmq/enabled_plugins:/etc/rabbitmq/enabled_plugins
            - ./rabbitmq/plugins/rabbitmq_delayed_message_exchange-3.11.1.ez:/plugins/rabbitmq_delayed_message_exchange-3.11.1.ez
            - rmqStorage:/var/lib/rabbitmq
        labels:
            - traefik.enable=true
            - traefik.http.routers.rabbitmq.rule=Host(`rabbitmq.docker.localhost`)
            - traefik.http.routers.rabbitmq.tls=true
            - traefik.http.services.rabbitmq.loadbalancer.server.port=15672

    mercure:
        image: dunglas/mercure:latest
        container_name: mercure
        networks:
            - traefik
        environment:
            SERVER_NAME: ":80"
            MERCURE_PUBLISHER_JWT_KEY: '!ChangeMe!'
            MERCURE_SUBSCRIBER_JWT_KEY: '!ChangeMe!'
        volumes:
            -   ./mercure/Caddyfile:/etc/caddy/Caddyfile
        labels:
            - traefik.enable=true
            - traefik.http.routers.mercure.rule=Host(`mercure.docker.localhost`)
            - traefik.http.routers.mercure.tls=true
            - traefik.http.services.mercure.loadbalancer.server.port=80

volumes:
    rmqStorage:

networks:
    traefik:
        external: true
